from services import MaterialScoreEvaluator
from chess import Board

material_score_calculator = MaterialScoreEvaluator()


def minimax(board, depth, maximizing_player):
    if depth == 0 or board.is_game_over():
        value = material_score_calculator.evaluate(board)
        if board.outcome():
            print('DEPTH: ', depth)
            print('GAME RESULT: ', board.outcome())
            print('MATERIAL SCORE: ', value)
            print(board)
        # print('MATERIAL SCORE', value)
        # print(board.fen())
        return value
    if maximizing_player:
        value = float('-inf')
        for move in board.legal_moves:
            board.push(move)
            value = max(value, minimax(board, depth - 1, False))
            board.pop()
        return value
    else:
        value = float('inf')
        for move in board.legal_moves:
            board.push(move)
            value = min(value, minimax(board, depth - 1, True))
            board.pop()
        return value


board_game = Board('2kr4/K1pp4/8/8/8/8/7Q/3R4 w k - 0 1')

minimax(board_game, 3, True)
