from chess import Board, Piece

from domain import Material


class MaterialScoreEvaluator:
    king_value: int
    queen_value: int
    rook_value: int
    knight_value: int
    bishop_value: int
    pawn_value: int

    def __init__(self):
        self.set_pieces_values()

    def evaluate(self, board: Board) -> int:
        white_pieces = []
        black_pieces = []
        for square in board.piece_map():
            piece: Piece = board.piece_map()[square]
            if piece.color:
                white_pieces.append(piece.symbol())
            else:
                black_pieces.append(piece.symbol())

        white_material = Material(white_pieces)
        black_material = Material(black_pieces)

        return self.king_value * (white_material.KING - black_material.KING) + \
               self.queen_value * (white_material.QUEEN - black_material.QUEEN) + \
               self.rook_value * (white_material.ROOK - black_material.ROOK) + \
               self.knight_value * (white_material.KNIGHT - black_material.KNIGHT) + \
               self.bishop_value * (white_material.BISHOP - black_material.BISHOP) + \
               self.pawn_value * (white_material.PAWN - black_material.PAWN)

    def set_pieces_values(self):
        self.king_value = 10000
        self.queen_value = 1000
        self.rook_value = 525
        self.knight_value = 350
        self.bishop_value = 350
        self.pawn_value = 100
