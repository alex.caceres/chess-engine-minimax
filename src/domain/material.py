class Material:
    KING: int = 0
    QUEEN: int = 0
    ROOK: int = 0
    KNIGHT: int = 0
    BISHOP: int = 0
    PAWN: int = 0

    def __init__(self, pieces=None):
        if pieces:
            for piece in pieces:
                if piece.upper() == 'K':
                    self.KING += 1
                if piece.upper() == 'Q':
                    self.QUEEN += 1
                if piece.upper() == 'R':
                    self.ROOK += 1
                if piece.upper() == 'N':
                    self.KNIGHT += 1
                if piece.upper() == 'B':
                    self.BISHOP += 1
                if piece.upper() == 'P':
                    self.PAWN += 1
        else:
            self.KING = 1
            self.QUEEN = 1
            self.ROOK = 2
            self.KNIGHT = 2
            self.BISHOP = 2
            self.PAWN = 8

    def kill_piece(self, piece):
        if getattr(self, piece) > 0:
            setattr(self, piece, getattr(self, piece) - 1)
            print('Killed {}'.format(piece))
        else:
            print('No more {} on the board'.format(piece))
